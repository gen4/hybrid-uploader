angular.module('starter.controllers')
	.service('FilePickerService', ['$rootScope','$q','$log','$window', function ($rootScope,$q,$log,$window) {


		function getThumb(fileExt, uri){
			var thumb;
			var fileKinds =[
				{
					id:'pdf',
					thumb:uri,
					exts:['pdf']
				},
				{
					id:'audio',
					thumb:'img/audio.png',
					exts:['mp3']
				},
				{
					id:'pic',
					thumb:uri,
					exts:['jpg','jpeg','png']
				}
			];

			_.some(fileKinds, function(kind){
				if(_.indexOf(kind.exts,fileExt)>=0){
					thumb = kind.thumb;
					return true;
				}else{
					return false;
				}
			});
			if(thumb){
				return thumb;
			}else{
				return 'img/ionic.png';
			}
		}



		function checkFileType(path, fileExt) {
			return path.match(new RegExp(fileExt + '$', 'i'));
		}

		function chooseFileOk(deferred, uri, fileExt) {
			$log.debug('FileManager#chooseFile - uri: ' + uri + ', fileType: ' + fileExt);
			var fileExt = _.takeRight(uri.split('.'))[0];
			//if (!checkFileType(uri, fileExt)) {
			//	deferred.reject('wrong_file_type');
			//} else {

				$window.resolveLocalFileSystemURL('file://'+uri,
					function(fileEntry){
						fileEntry.getMetadata(function(metadata) {
							deferred.resolve({
								url:'file://'+uri,
								thumb:getThumb(fileExt,uri),
								fileExt:fileExt,
								size:metadata.size
							});
						});
					}, function(error){
					deferred.reject(error);
				});
			//}
		}

		function chooseFileError(deferred, source) {
			$log.debug('FileManager#chooseFile - ' + source + ' error: ' + JSON.stringify(error));

			// assume operation cancelled
			deferred.reject('cancelled');
		}

		var chooseFile = function (fileExt) {
			console.log('CF');
			var deferred = $q.defer();

			// iOS (NOTE - only iOS 8 and higher): https://github.com/jcesarmobile/FilePicker-Phonegap-iOS-Plugin
			if (ionic.Platform.isIOS()) {
				console.log('ios',FilePicker);
				FilePicker.pickFile(
					function (uri) {
						chooseFileOk(deferred, uri, fileExt);
					},
					function (error) {
						chooseFileError(deferred, 'FilePicker');
					}
				);

				// Android: https://github.com/don/cordova-filechooser
			} else {
				console.log('andr',fileChooser);
				fileChooser.open(
					function (uri) {
						chooseFileOk(deferred, uri, fileExt);
					},
					function (error) {
						chooseFileError(deferred, 'fileChooser');
					}
				);
			}
			return deferred.promise;
		}

		return {
			chooseFile:chooseFile
		}
	}]);




