angular.module('starter.controllers')
	.service('categoriesService', ['$rootScope', function ($rootScope) {
		return [
				{
					title: 'Category 1',
					id:'cat1',
					documents: [
						{
							id:'doc1',
							title: 'Document Type 1'
						},
						{
							id:'doc2',
							title: 'Document Type 2'
						}
					]
				},
				{
					title: 'Category 2',
					id:'cat2',
					documents: [
						{
							id:'doc3',
							title: 'Document Type 3'
						},
						{
							id:'doc4',
							title: 'Document Type 4'
						}
					]
				}
			];
	}]);


