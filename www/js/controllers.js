angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  //// Form data for the login modal
  //$scope.loginData = {};
  //
  //// Create the login modal that we will use later
  //$ionicModal.fromTemplateUrl('templates/upload.html', {
  //  scope: $scope
  //}).then(function(modal) {
  //  $scope.modal = modal;
  //});
  //
  //// Triggered in the login modal to close it
  //$scope.closeUpload = function() {
  //  $scope.modal.hide();
  //};
  //
  //// Open the login modal
  //$scope.upload = function() {
  //  $scope.modal.show();
  //};

  // Perform the login action when the user submits the login form

})

.controller('MainCtrl', function($scope,categoriesService,$ionicModal,FilePickerService) {
var filesUploaded = 1;
  $scope.importedFiles = getRandomFiles(12);
  $scope.moment = moment;
  processList();

  function processList(){
    $scope.importedFilesByCat = _.groupBy($scope.importedFiles,'categoryType');
    $scope.categoriesService = categoriesService;
    $scope.getTitleByGroupId = function(gid){
      return _.find(categoriesService,{id:gid}).title;
    };
  }

  function getRandomFiles(qty){
    return _.map(new Array(qty), function(item, i){
      var cat = _.sample(categoriesService);
      var doc = _.sample(cat.documents);
      var size = (Math.round(Math.random()*99999)*((Math.random()>0.5)?1:1000));
      var date = moment().subtract(Math.pow(Math.random(),5)*70000, 'seconds');
      return {
        title:'File number '+i,
        date:date,
        size: filesize(size),
        thumb:'img/ionic.png',
        categoryType:cat.id,
        documentType:doc.id
      }
    });
  }




  $scope.setUploadingDocCatType = function(cat){
    $scope.uploadingDoc.categoryType = cat;
  };
  $scope.removeUploadingDocCatType = function(){
    $scope.uploadingDoc.categoryType = null;
    $scope.uploadingDoc.documentType = null;
  };
  $scope.setUploadingDocDocType = function(doc){
    $scope.uploadingDoc.documentType = doc;
  };
  $scope.removeUploadingDocDocType = function(){
    $scope.uploadingDoc.documentType = null;
  };

  $scope.confirmUploadingDoc = function(){

    $scope.uploadingDoc.categoryType = $scope.uploadingDoc.categoryType.id;
    $scope.uploadingDoc.documentType = $scope.uploadingDoc.documentType.id;
    $scope.importedFiles.push($scope.uploadingDoc);
    processList();
    $scope.modal.hide();
    $scope.uploadingDoc = null;
    filesUploaded++;
  }
  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/upload.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeUpload = function() {
    $scope.modal.hide();
  };

  $scope._upload = function(fileObj) {
    $scope.uploadingDoc = getRandomFiles(1)[0];
    $scope.uploadingDoc.title = _.takeRight(fileObj.url.split('/'))[0];
    $scope.uploadingDoc.size = filesize(fileObj.size);
    $scope.uploadingDoc.thumb =fileObj.thumb;
    $scope.uploadingDoc.date = new Date().getTime();

    console.log($scope.uploadingDoc);
    delete $scope.uploadingDoc.categoryType;
    delete $scope.uploadingDoc.documentType;
    $scope.modal.show();
  };

  // Open the login modal
  $scope.upload = function() {
    FilePickerService.chooseFile('.jpeg').then(function(fileObj) {
      console.log('url',fileObj);
      $scope._upload(fileObj)
    }).catch(function(error) {
      console.log('err',error);
    });
  };
});

